const axios = require('axios');
const fs = require('fs');

const baseSrcPath = 'https://gitlab.com/gitlab-org/gitlab/-/raw/master';

const jsonSrcPath = `${baseSrcPath}/fixtures/emojis`;
const jsonDestPath = 'src/behaviors/emoji/json';
const jsonFilenames = ['digests.json', 'aliases.json'];

const imgSrcPath = `${baseSrcPath}/app/assets/images/emoji`;
const imgDestPath = 'public/images/emoji';

const createDirIfNotExists = (dirPath) => {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true });
  }
};

createDirIfNotExists(jsonDestPath);

const jsonFiles = jsonFilenames.map((filename) => {
  const file = `${jsonSrcPath}/${filename}`;

  return axios
    .get(file)
    .then((response) => {
      fs.writeFile(`${jsonDestPath}/${filename}`, JSON.stringify(response.data), (err) => {
        if (err) throw err;
        console.log(`Saved ${filename}`);
      });
      return response.data;
    })
    .catch((error) => console.log(`Failed copying: ${file}: ${error}`));
});

Promise.all(jsonFiles).then(([digests]) => {
  createDirIfNotExists(imgDestPath);
  const emojiNames = Object.keys(digests);
  emojiNames.forEach((emoji) => {
    const srcFile = `${imgSrcPath}/${emoji}.png`;
    const destFile = `${imgDestPath}/${emoji}.png`;

    if (!fs.existsSync(destFile)) {
      axios
        .get(srcFile, { responseType: 'stream' })
        .then((response) => response.data.pipe(fs.createWriteStream(destFile)))
        .catch((error) => console.log(`Failed copying: ${emoji}.png: ${error}`));
    }
  });
});
