import { shallowMount } from '@vue/test-utils';
import App from '~/app.vue';

describe('App.vue', () => {
  it('renders main components', () => {
    const wrapper = shallowMount(App, {
      stubs: ['router-link', 'router-view'],
    });
    expect(wrapper).toBeInstanceOf(Object);
  });
});
